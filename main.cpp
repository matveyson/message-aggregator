#include <iostream>
#include <args.hxx>

#define CONFIGURU_IMPLEMENTATION 1
#define CONFIGURU_IMPLICIT_CONVERSIONS 1

#include <redox.hpp>
#include <sio_client.h>
#include <configuru.hpp>
#include <cpprest/http_client.h>
#include <cpprest/filestream.h>

/*class Log {
private:
    char level = 0;
    static char currentlevel = 0;
public:
    Log(char level) {
        this->level = level;
    }

    static void setCurrentLevel(const char level) {
        currentlevel = level;
    }

    template<typename T>
    Log &operator<<(const T &text) {
        if (level >= currentlevel)
            std::cout << text;
        return *this;
    }
} Log(0), VerboseLog(1), VVerboseLog(2);*/

class MessageAggregator {
    struct source {
        std::string name;
        enum type {vk_sio, telegram_sio};
        std::string host;
        unsigned port;
    };
    struct route {
        std::string match;
        std::string route;
    };
    struct destination{
        enum type {redis};
        std::string host;
        unsigned port;
    };

private:
    std::vector<source> sources;
    std::vector<route> routes;
    std::vector<destination> destinations;
public:
    void parseConfig(const configuru::Config &config){

    }
    void addSource(const source &s){
      this->sources.push_back(s);
    };
    void addRoute(const route &r){
      this->routes.push_back(r);
    };
    void addDestination(const destination &d){
      this->destinations.push_back(d);
    };
    void processMessage(){

    }
};

//TODO: refactor
void start_vk_service(const configuru::Config &config) {
    std::cout << config["name"] << ": connecting" << std::endl;

    sio::client& h( * new sio::client()); //TODO: FIX LEAK
    h.set_open_listener([&]() {
        std::cout << config["name"] << ": connected! Listening..." << std::endl;
        h.socket()->on("check", [&](sio::event &event) {
            std::cout << event.get_name() << ":" << event.get_message()->get_map()["text"]->get_string() << std::endl;
        });
    });
    h.connect("http://"+ config["connection"]["host"].as_string() + ":" + std::to_string((int)config["connection"]["port"]));
};

void add_source(const configuru::Config &element) {
    std::cout << "Found source: (name: " << element["name"] << ", type: " << element["type"] << ")" << std::endl;
    if (element["type"] == "vk.sio")
        if (element.has_key("connection") and
            element["connection"].has_key("host") and element["connection"]["host"].is_string() and
            element["connection"].has_key("port") and element["connection"]["port"].is_int())
            start_vk_service(element);
        else {
            std::cerr << "Service type " << element["type"] << " is not supported!" << std::endl;
            std::cout << "Skipping service " << element["name"] << std::endl;
        }
    else {
        std::cerr << "Service type " << element["type"] << " is not supported!" << std::endl;
        std::cout << "Skipping service " << element["name"] << std::endl;
    }
};

void add_route(const configuru::Config &element) {
    std::cout << "Found route: (match: " << element["match"] << ", to channel: " << element["channel"]
              << ")" << std::endl;
    return;
}

void add_destination(const configuru::Config &element) {
    std::cout << "Destination type: " << element["destination"]["type"] << std::endl;

    std::cout << "REDIS: connecting" << std::endl;
    static redox::Redox rdx; //TODO: FIX
    if (!rdx.connect(element["destination"]["connection"]["host"].as_string(),
                     (int) element["destination"]["connection"]["port"])) {
        std::cerr << "Redis connection failure!" << std::endl;
        std::exit(1);
    };

    std::cout << "REDIS: connected" << std::endl;
    rdx.set("hello", "world!");
    rdx.publish("test", "testing");
    std::cout << "Hello, " << rdx.get("hello") << std::endl;
    //rdx.disconnect();
    std::cout << "REDIS: OK" << std::endl;

    return;
};

int main(int argc, const char **argv) {
    args::ArgumentParser parser(
            "This is a real-time message aggregator that helps you to push messages form various services to your Redis queue respecting custom channels and filters");
    args::HelpFlag help(parser, "help", "Display this help menu", {'h', "help"});
    args::ValueFlag<std::string> config(parser, "config", "path to configuration file", {'c', "config"},
                                        "./config.json");
    args::Flag use_env(parser, "use_env", "Parse credentials from ENVIRONMENT variables", {'e', "env"});
    args::Group group(parser, "Verbosity:", args::Group::Validators::AtMostOne);
    args::Flag verbose_level_1(group, "verbose_level_1", "Verbose", {'v'});
    args::Flag verbose_level_2(group, "verbose_level_2", "Xtra verbose", {"vv"});

    try {
        parser.ParseCLI(argc, argv);
    }
    catch (args::Help &e) {
        std::cout << parser;
        return 0;
    }
    catch (args::ParseError &e) {
        std::cerr << e.what() << std::endl;
        std::cerr << parser;
        return 1;
    }
    catch (args::ValidationError &e) {
        std::cerr << e.what() << std::endl;
        std::cerr << parser;
        return 1;
    }
    std::cout << "Starting service..." << std::endl;
    if (verbose_level_1 || verbose_level_2) {
        std::cout << "Changed verbosity to " << (verbose_level_1 ? "LOW" : "HIGH") << std::endl;
    }
    if (use_env) { std::cout << "Using credentials from ENV" << std::endl; }
    std::cout << "Loading configuration file (" << args::get(config) << ") ..." << std::endl;
    configuru::Config cfg;
    try {
        cfg = configuru::parse_file(args::get(config), configuru::JSON);
        //DESTINATIONS
        std::cout << "\n==LOADING DESTINATIONS==" << std::endl;
        add_destination(cfg);

        //ROUTES
        std::cout << "\n==LOADING ROUTES==" << std::endl;
        if (cfg["routes"].is_array())
            for (const configuru::Config &element : cfg["routes"].as_array())
                add_route(element);

        //SOURCES
        std::cout << "\n==LOADING SOURCES==" << std::endl;
        if (cfg["sources"].is_array())
            for (const configuru::Config &element : cfg["sources"].as_array())
                add_source(element);
    }
    catch (const std::runtime_error &error) {
        std::cerr << error.what() << std::endl;
        return 1;
    }
    std::cout << "\nConfiguration file loaded" << std::endl;
    std::cout << "Service started!" << std::endl;

    while (1){
        sleep(1);
    }
    //condition_variable
    //boost asio
    return 0;
}