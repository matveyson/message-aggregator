# About #

This project helps you to grab new messages and notification from several services and push them to your redis pubsub.

### What services are supported? ###

* [VK](https://vk.com)
* [Asana](https://asana.com)
* [Travis CI](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact